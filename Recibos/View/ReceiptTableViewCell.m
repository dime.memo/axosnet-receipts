//
//  ReceiptTableViewCell.m
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/29/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import "ReceiptTableViewCell.h"

@implementation ReceiptTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
