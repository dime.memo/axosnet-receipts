//
//  Utils.m
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/30/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import "Utils.h"

@implementation Utils
- (void) showAlert: (UIViewController *) viewController alert: (NSString *) message with: (NSArray<UIAlertAction *>*) actions  {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Recibos" message:message preferredStyle:UIAlertControllerStyleAlert];
    for (UIAlertAction *action in actions) {
        [alert addAction:action];
    }
    [viewController presentViewController:alert animated:true completion:nil];
}
@end
