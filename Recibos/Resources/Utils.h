//
//  Utils.h
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/30/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject
- (void) showAlert: (UIViewController *) viewController alert: (NSString *) message with: (NSArray<UIAlertAction *>*) actions;
@end

NS_ASSUME_NONNULL_END
