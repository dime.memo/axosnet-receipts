//
//  CurrencyCode.m
//  Recibos
//
//  Created by Memo Rodriguez on 5/30/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import "CurrencyCode.h"

@implementation CurrencyCode

// Main initializer
- (instancetype)initWithName:(NSString *)currencyCode
                    codeDescription:(NSString *)codeDescription {
    
    self = [super init];
    if (self){
        _currencyCode = currencyCode;
        _codeDescription = codeDescription;
    }
    return self;
}

// Gets dictionary values and return Receipts object
- (instancetype)initWithName:(NSDictionary *)receipts {
    NSString *currencyCode = receipts[@"currency_code"];
    NSString *codeDescription = receipts[@"description"];
    
    self = [super  init];
    if (self){
        _currencyCode = currencyCode;
        _codeDescription = codeDescription;
    }
    return self;
}


@end
