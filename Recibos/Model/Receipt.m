//
//  Courses.m
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/29/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import "Receipt.h"

@implementation Receipt

// Main initializer
- (instancetype)initWithName:(NSNumber *)receiptId
                    provider:(NSString *)provider
                      amount:(NSDecimalNumber *)amount
                emissionDate:(NSString *)emissionDate
                     comment:(NSString *)comment
                currencyCode:(NSString *)currencyCode {
    
    self = [super init];
    if (self){
        _receiptId = receiptId;
        _provider = provider;
        _amount = amount;
        _emissionDate = emissionDate;
        _comment = comment;
        _currencyCode = currencyCode;
    }
    return self;
}

// Gets dictionary values and return Receipts object
- (instancetype)initWithName:(NSDictionary *)receipts {
    NSNumber *receiptId = receipts[@"id"];
    NSString *provider = receipts[@"provider"];
    NSDecimalNumber *amount = receipts[@"amount"];
    NSString *emissionDate = receipts[@"emission_date"];
    NSString *comment = receipts[@"comment"];
    NSString *currencyCode = receipts[@"currency_code"];
    
    self = [super  init];
    if (self){
        _receiptId = receiptId;
        _provider = provider;
        _amount = amount;
        _emissionDate = [emissionDate componentsSeparatedByString:@" "].firstObject;
        _comment = comment;
        _currencyCode = currencyCode;
    }
    return self;
}

- (NSString *)ammoutFormatted {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    return [NSString stringWithFormat:@"%@ %@",[formatter stringFromNumber: _amount], _currencyCode];
}

@end
