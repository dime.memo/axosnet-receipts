//
//  Courses.h
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/29/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Receipt : NSObject

@property (strong, nonatomic) NSNumber *receiptId;
@property (strong, nonatomic) NSString *provider;
@property (strong, nonatomic) NSDecimalNumber *amount;
@property (strong, nonatomic) NSString *emissionDate;
@property (strong, nonatomic) NSString *comment;
@property (strong, nonatomic) NSString *currencyCode;


- (instancetype)initWithName:(NSNumber *)receiptId
                    provider:(NSString *)provider
                      amount:(NSDecimalNumber *)amount
                emissionDate:(NSString *) emissionDate
                     comment:(NSString *)comment
                currencyCode:(NSString *) currencyCode;

- (instancetype)initWithName:(NSDictionary *)receipts;

- (NSString *) ammoutFormatted;

@end

NS_ASSUME_NONNULL_END
