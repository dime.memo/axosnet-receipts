//
//  CurrencyCode.h
//  Recibos
//
//  Created by Memo Rodriguez on 5/30/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CurrencyCode : NSObject

@property (strong, nonatomic) NSString *currencyCode;
@property (strong, nonatomic) NSString *codeDescription;

- (instancetype)initWithName:(NSString *)currencyCode
                    codeDescription:(NSString *)codeDescription;

- (instancetype)initWithName:(NSDictionary *)currencyCodes;

@end

NS_ASSUME_NONNULL_END
