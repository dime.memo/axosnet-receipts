//
//  NewReceiptViewController.m
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/30/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//


#import "XLForm.h"
#import "XLFormViewController.h"
#import "NewReceiptViewController.h"
#import "Utils.h"

NSString *const kProvider = @"provider";
NSString *const kEmissionDate = @"emissionDate";
NSString *const kAmmount = @"amount";
NSString *const kCurrencyCode = @"currencyCode";
NSString *const kNotes = @"notes";

@interface CurrencyFormatter : NSNumberFormatter

@property (readonly, strong) NSDecimalNumberHandler *roundingBehavior;

@end

@implementation CurrencyFormatter

Utils *util;

- (id) init{
    self = [super init];
    if (self) {
        [self setNumberStyle: NSNumberFormatterCurrencyStyle];
        [self setGeneratesDecimalNumbers:YES];
        [self setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];

        
        NSUInteger currencyScale = [self maximumFractionDigits];
        
        _roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:currencyScale raiseOnExactness:FALSE raiseOnOverflow:TRUE raiseOnUnderflow:TRUE raiseOnDivideByZero:TRUE];
        
    }
    
    return self;
}
@end

@implementation NewReceiptViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self configureViewDidLoad];
    [self configureForm];
}

-(void) configureViewDidLoad{
    self.navigationItem.title = @"Recibo";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(savePressed:)];
    self.tableView.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:251.0/255.0 blue:251.0/255.0 alpha:1];
    
    util = Utils.new;
}

-(id)configureForm{
    
    XLFormDescriptor * formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@"Text Fields"];
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    formDescriptor.assignFirstResponderOnShow = YES;
    
    // Basic Information - Section
    section = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section];
    
    // Name
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kProvider rowType:XLFormRowDescriptorTypeText title:@"Emisor:"];
    row.required = YES;
    row.value = _isNewReceipt ? @"" : _receipt.provider;
    [section addFormRow:row];

    // Emission Date
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kEmissionDate rowType:XLFormRowDescriptorTypeDateTime title:@"Fecha de emision:"];
    row.required = YES;
    row.value = [NSDate new];
    [section addFormRow:row];
    
    // Ammount
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kAmmount rowType:XLFormRowDescriptorTypeDecimal title:@"Cantidad:"];
    row.required = YES;
    CurrencyFormatter *numberFormatter = [[CurrencyFormatter alloc] init];
    row.valueFormatter = numberFormatter;
    row.value = [NSDecimalNumber numberWithDouble:  _isNewReceipt ? 0.0 :_receipt.amount.doubleValue];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [section addFormRow:row];
    
    // Currency code
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kCurrencyCode rowType:XLFormRowDescriptorTypeSelectorPush title:@"Divisa:"];
    row.selectorOptions = _currencyCodes;
    //row.value = [_currencyCodes objectAtIndex:@(0)];
    [section addFormRow:row];
    
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Si tienes algun comentario escribelo aqui abajo"];
    [formDescriptor addFormSection:section];
    
    // Comments
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kNotes rowType:XLFormRowDescriptorTypeTextView title:@"Comentarios:"];
    row.required = YES;
    row.value =  _isNewReceipt ? @"" : _receipt.comment;
    [section addFormRow:row];
    
    return [super initWithForm:formDescriptor];
    
}

-(void)savePressed:(UIBarButtonItem * __unused)button {
    
    // Validating the form
    NSArray * validationErrors = [self formValidationErrors];
    if (validationErrors.count > 0){
        [self showFormValidationError:[validationErrors firstObject]];
        return;
    }
    
    [self.tableView endEditing:YES];
    [self updateReceipt];
}

-(void)updateReceipt{
    
    // Actions for UIAlertController
    NSMutableArray<UIAlertAction *> *actions = NSMutableArray.new;
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Entendido" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:true];
    }];
    [actions addObject:ok];
    
    // Getting data from the form
    NSString *provider = [self.formValues valueForKey:kProvider];
    NSString *comments = [self.formValues valueForKey:kNotes];
    XLFormOptionsObject *currencyCode = [self.formValues valueForKey:kCurrencyCode];
    NSDate *date = [self.formValues valueForKey:kEmissionDate];
    NSString *emissionDate = [self formatDate:date];
    NSDecimalNumber *ammount = [self.formValues valueForKey:kAmmount];
    NSString *currencyCodeString = [currencyCode formDisplaytext];
    
    // create string URL
    NSString *stringURL = _isNewReceipt ? [NSString stringWithFormat:@"https://devapi.axosnet.com/am/v2/api_receipts_beta/api/receipt/insert?provider=%@&amount=%f&comment=%@&emission_date=%@&currency_code=%@",provider, ammount.floatValue, comments, emissionDate, currencyCodeString] : [NSString stringWithFormat:@"https://devapi.axosnet.com/am/v2/api_receipts_beta/api/receipt/update?id=%@&provider=%@&amount=%f&comment=%@&emission_date=%@&currency_code=%@",_receipt.receiptId, provider, ammount.floatValue, comments, emissionDate, currencyCodeString];
    NSString *escapedPath = [stringURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // create URL request
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: escapedPath]];
    
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"POST"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (httpResponse.statusCode == 200) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *successMessage = self.isNewReceipt ? @"Se guardo correctamente el recibo.": @"Se actualizo correctamente la información";
                    [util showAlert:self alert:successMessage with:actions];
                });
            }
            else {
                // back to main thread and update the UI
                NSLog(@"%@", error);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [util showAlert:self alert:@"Ocurrio un error, intenta nuevamente." with:actions];
                });
            }
        }];
        [dataTask resume];
    });
}

-(NSString *) formatDate: (NSDate *) date{
    // Formating the selected date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM- dd"];
    return  [dateFormatter stringFromDate:date];
}

@end
