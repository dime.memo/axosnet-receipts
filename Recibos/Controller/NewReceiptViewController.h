//
//  NewReceiptViewController.h
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/30/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import "XLFormViewController.h"
#import "Receipt.h"
#import "CurrencyCode.h"
NS_ASSUME_NONNULL_BEGIN

@interface NewReceiptViewController : XLFormViewController
@property (nonatomic, assign) Receipt *receipt;
@property (nonatomic, assign) Boolean *isNewReceipt;
@property (nonatomic, assign) NSMutableArray<XLFormOptionsObject *> *currencyCodes;

@end

NS_ASSUME_NONNULL_END
