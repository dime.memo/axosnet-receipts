//
//  ViewController.h
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/29/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLForm.h"
#import "XLFormViewController.h"
#import "Receipt.h"
#import "CurrencyCode.h"

@interface ReceiptsViewController : UITableViewController 
@property (strong, nonatomic) NSMutableArray<Receipt *> *receipts;
@property (strong, nonatomic) NSMutableArray<XLFormOptionsObject *> *currencyCodes;
@end

