//
//  ViewController.m
//  Objective-c-test
//
//  Created by Memo Rodriguez on 5/29/19.
//  Copyright © 2019 Guillermo Rodriguez Morales. All rights reserved.
//

#import "ReceiptsViewController.h"
#import "ReceiptTableViewCell.h"
#import "NewReceiptViewController.h"
#import "Utils.h"

@implementation ReceiptsViewController

NSString *cellId = @"cellId";
UIActivityIndicatorView *spinner;
Utils *utils;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureNavigationBar];
    [self configureSpinner];
    [self fetchAllCurrencyCodes];
}

- (void)viewWillAppear:(BOOL)animated {
    [self fetchAllReceipts];
}

- (IBAction)newReceipt:(id)sender {
}


-(void)configureNavigationBar {
    self.navigationItem.title = @"Recibos";
    self.navigationController.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
    self.navigationController.navigationBar.barTintColor = UIColor.whiteColor;
    
    utils = [[Utils alloc] init];
}

-(void)configureSpinner {
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.tableView.backgroundView = spinner;
}

- (void)confirmReceiptDeletion: (Receipt *) receipt at: (NSIndexPath *) indexPath {
    NSMutableArray<UIAlertAction *> *actions = NSMutableArray.new;
    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Si" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self removeReceiptFromServer:receipt at:indexPath];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
    [actions addObject:cancel];
    [actions addObject:yes];
    [utils showAlert:self alert:@"¿Deseas eliminar este recibo?" with:actions];
}

-(void)fetchAllReceipts {
    // create string URL
    NSString *urlString = @"https://devapi.axosnet.com/am/v2/api_receipts_beta/api/receipt/getall";
    // create URL request
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: urlString]];
    
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"GET"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //back to the main thread for the UI call
        dispatch_async(dispatch_get_main_queue(), ^{
            [spinner startAnimating];
        });
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (httpResponse.statusCode == 200) {
                
                NSError *err;
                NSString *dataToString = [NSJSONSerialization JSONObjectWithData: data
                                                                         options: NSJSONReadingAllowFragments
                                                                           error:&err];
                
                NSData *JSONData = [dataToString dataUsingEncoding:NSUTF8StringEncoding];
                NSArray *JSON = [NSJSONSerialization JSONObjectWithData: JSONData
                                                                options: NSJSONReadingAllowFragments
                                                                  error:&err];
                
                if (err) {
                    // back to main thread and update the UI
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray<UIAlertAction *> *actions = NSMutableArray.new;
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Entendido" style:UIAlertActionStyleCancel handler:nil];
                        [actions addObject:ok];
                        [utils showAlert:self alert:@"Error al caragar los recibos, intenta nuevamente." with:actions];
                    });
                }
                
                NSMutableArray<Receipt *> *courses = NSMutableArray.new;
                // Iterate over the array to get the values
                for (NSDictionary *dict in JSON) {
                    Receipt *receipt = [[Receipt alloc] initWithName:dict];
                    [courses addObject:receipt];
                }
                
                // Order the array descending
                NSArray *orderedArray = [courses sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                    NSString *first = [(Receipt *)a emissionDate];
                    NSString *second = [(Receipt *)b emissionDate];
                    return [first compare:second];
                }];
                
                // Cast and set the ordered array
                self.receipts = [NSMutableArray arrayWithArray:orderedArray];
                //back to the main thread for the UI call
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                    [spinner stopAnimating];
                });
            }
            else {
                // back to main thread and update the UI
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableArray<UIAlertAction *> *actions = NSMutableArray.new;
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Entendido" style:UIAlertActionStyleCancel handler:nil];
                    [actions addObject:ok];
                    [utils showAlert:self alert:@"Error al caragar los recibos, intenta nuevamente." with:actions];
                });
            }
        }];
        [dataTask resume];
    });
}

-(void)removeReceiptFromServer: (Receipt *) receipt at: (NSIndexPath *) indexPath {
    
    // create string URL
    NSString *stringURL = [NSString stringWithFormat:@"https://devapi.axosnet.com/am/v2/api_receipts_beta/api/receipt/delete?id=%@", receipt.receiptId];
    // create URL request
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: stringURL]];
    
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"POST"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (httpResponse.statusCode == 200) {
                // back to main thread and update the UI
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.receipts removeObjectAtIndex: indexPath.row];
                    [self.tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath]
                                          withRowAnimation:UITableViewRowAnimationFade];
                });
            }
            else {
                // back to main thread and update the UI
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableArray<UIAlertAction *> *actions = NSMutableArray.new;
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Entendido" style:UIAlertActionStyleCancel handler:nil];
                    [actions addObject:ok];
                    [utils showAlert:self alert:@"Error al eliminar el recibo, intenta nuevamente." with:actions];
                });
            }
        }];
        [dataTask resume];
    });
}

-(void)fetchAllCurrencyCodes {
    // create string URL
    NSString *urlString = @"https://devapi.axosnet.com/am/v2/api_receipts_beta/api/currency/getall";
    // create URL request
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: urlString]];
    
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"GET"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (httpResponse.statusCode == 200) {
                
                NSError *err;
                NSString *dataToString = [NSJSONSerialization JSONObjectWithData: data
                                                                         options: NSJSONReadingAllowFragments
                                                                           error:&err];
                
                NSData *JSONData = [dataToString dataUsingEncoding:NSUTF8StringEncoding];
                NSArray *JSON = [NSJSONSerialization JSONObjectWithData: JSONData
                                                                options: NSJSONReadingAllowFragments
                                                                  error:&err];
                
                if (err) {
                    // back to main thread and update the UI
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray<UIAlertAction *> *actions = NSMutableArray.new;
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Entendido" style:UIAlertActionStyleCancel handler:nil];
                        [actions addObject:ok];
                        [utils showAlert:self alert:@"Error al caragar los recibos, intenta nuevamente." with:actions];
                    });
                }
                
                NSUInteger index = 0;
                NSMutableArray<XLFormOptionsObject *> *currencyCodes = NSMutableArray.new;
                // Iterate over the array to get the values
                for (NSDictionary *dict in JSON) {
                    CurrencyCode *currencyCode = [[CurrencyCode alloc] initWithName:dict];
                    [currencyCodes addObject: [XLFormOptionsObject formOptionsObjectWithValue:@(index) displayText:currencyCode.currencyCode]];
                    index ++;
                }
                
                //back to the main thread for the UI call
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.currencyCodes = currencyCodes;
                });
            }
            else {
                // back to main thread and update the UI
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableArray<UIAlertAction *> *actions = NSMutableArray.new;
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Entendido" style:UIAlertActionStyleCancel handler:nil];
                    [actions addObject:ok];
                    [utils showAlert:self alert:@"Error al caragar los recibos, intenta nuevamente." with:actions];
                });
            }
        }];
        [dataTask resume];
    });
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NewReceiptViewController *newReceiptViewController = (NewReceiptViewController *)segue.destinationViewController;
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    newReceiptViewController.currencyCodes = self.currencyCodes;
    if (indexPath) {
        newReceiptViewController.receipt = [self.receipts objectAtIndex:indexPath.row];
        newReceiptViewController.isNewReceipt = NO;
    } else {
        newReceiptViewController.isNewReceipt = YES;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.receipts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReceiptTableViewCell *cell =  [tableView
                                   dequeueReusableCellWithIdentifier:cellId
                                   forIndexPath:indexPath];
    Receipt *receipt = self.receipts[indexPath.row];
    cell.providerLabel.text = receipt.provider;
    cell.amountLabel.text = receipt.ammoutFormatted;
    cell.emissionDateLabel.text = receipt.emissionDate;
    cell.commentLabel.text = receipt.comment;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    Receipt *receipt = Receipt.new;
    receipt = [self.receipts objectAtIndex:indexPath.row];
    [self confirmReceiptDeletion:receipt at:indexPath];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

@end
