Requisitos para correl el proyecto:
1.- CocoaPods.
2.- Xcode 10.

Como instalar CocoaPods:
1.- Abrir la terminal.
2.- Ejecutar el siguiente comando "sudo gem install cocoapods".
3.- Para mas informacion de lo que es CocoaPods pueden revisar el siguiente link https://guides.cocoapods.org/using/getting-started.html.

Como instalar el project:
1.- Clonar el repositorio "axosnet-receipts".
2.- Abrir la terminal e ir a la carpeta root del proyecto.
3.- Ejecutar el comando "pod install".
4.- Abir el proyecto "Recibos.xcworkspace" en Xcode.
5.- Por ultimo ejecutar en el teclado cmd + b para verificar que el proyecto compile correctamente.

NOTA: en caso de que se tenga algun error en los pods:
1.- Eliminar la carpeta Pods, Podfile.lock y Recibos.xcworkspace
2.- Ejecutar el comando "pod install".

Para el formulario utilice la siguiente libreria.
1.-https://github.com/xmartlabs/XLForm
Es una excelente opcion para formularios con multiples campos de diferente tipo.